package pizzashop.food;

public enum Flavor{
	COKE("Coke"),
	PEPSI("Pepsi"),
	SPRITE("Sprite"),
	ORANGE("Orange"),
	WATER("Water"),
	COFFEE("Coffee");
	
	Flavor(String name) { this.name = name; }
	public String toString() { return name; }
	private String name;
}
