package pizzashop.food;

public abstract class AbstractItem {
	protected int size;
	public static final String [] sizes = { "None", "Small", "Medium", "Large" };
	public AbstractItem(int size){
		this.size = size;
	}
	public String getSize(){
		if(size<sizes.length&&size>0){
			return sizes[size];
		}
		return null;
	}
}
